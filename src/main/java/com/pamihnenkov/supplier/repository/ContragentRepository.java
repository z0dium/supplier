package com.pamihnenkov.supplier.repository;

import com.pamihnenkov.supplier.model.Contragent;
import org.springframework.data.repository.CrudRepository;

public interface ContragentRepository extends CrudRepository<Contragent, Long> {
}
