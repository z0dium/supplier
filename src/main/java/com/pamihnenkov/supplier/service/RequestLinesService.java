package com.pamihnenkov.supplier.service;

import com.pamihnenkov.supplier.model.RequestLine;

public interface RequestLinesService extends CrudService<RequestLine,Long> {
}
