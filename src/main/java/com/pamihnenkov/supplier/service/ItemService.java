package com.pamihnenkov.supplier.service;
import com.pamihnenkov.supplier.model.Item;

public interface ItemService extends CrudService<Item, Long>{
}
