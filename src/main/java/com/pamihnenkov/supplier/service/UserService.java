package com.pamihnenkov.supplier.service;

import com.pamihnenkov.supplier.model.User;

public interface UserService extends CrudService<User, Long>{

}
